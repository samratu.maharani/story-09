from django.test import TestCase
from django.test import TestCase, Client
from django.urls import resolve
from django.apps import apps
from .views import *
from .apps import *
import unittest

class UnitTest(TestCase):
    def test_profile_url_is_exist(self):
        response = Client().get('')
        self.assertEqual(response.status_code, 302)
    def test_url_is_notexist(self):
        response = Client().get('/notexist/')
        self.assertEqual(response.status_code, 404)
    def test_login_url_is_exist(self):
        response = Client().get('/login/')
        self.assertEqual(response.status_code, 200)
    def test_logout_url_is_exist(self):
        response = Client().get('/logout/')
        self.assertEqual(response.status_code, 200)
    def test_resgiter_url_is_exist(self):
        response = Client().get('/register/')
        self.assertEqual(response.status_code, 200)
    def test_using_profile_func(self):
        found = resolve("/")
        self.assertEqual(found.func, profile)
    def test_using_register_func(self):
        found = resolve("/register/")
        self.assertEqual(found.func, register)

